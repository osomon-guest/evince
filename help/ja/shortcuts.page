<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shortcuts" xml:lang="ja">

  <info>
    <link type="guide" xref="index#advanced"/>
    <desc>See a list of all shortcuts.</desc>

    <revision pkgversion="3.2" version="0.1" date="2011-09-05" status="final"/>
    <revision pkgversion="3.24" version="0.2" date="2017-08-10" status="final"/>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
       <name>Tiffany Antopolski</name>
       <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Taufan Lubis</name>
       <email>taufanlinux@gmail.com</email>
    </credit>
   <credit type="editor">
       <name>Ronaldi Santosa</name>
       <email>ronald.santosa@gmail.com</email>
    </credit>
   <credit type="editor">
       <name>Ibnu Amansyah</name>
       <email>javalander9@gmail.com</email>
    </credit>
   <credit type="editor">
       <name>Andre Klapper</name>
       <email>ak-47@gmx.net</email>
    </credit>

    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>

  </info>


<title>キーボードショートカット</title>

<section id="default-shortcuts"><title>デフォルトのショートカット</title>
<section id="open-close-save-print">
  <title>開く、閉じる、保存および印刷</title>
  <table frame="all" rules="cols" shade="rows">
    <tr>
        <td><p>ドキュメントを開く</p></td>
        <td><p><keyseq><key>Ctrl</key><key>O</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>現在のドキュメントのコピーを開く</p></td>
        <td><p><keyseq><key>Ctrl</key><key>N</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>現在のドキュメントのコピーを新しい名前で保存する</p></td>
        <td><p><keyseq><key>Ctrl</key><key>S</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>現在のドキュメントを印刷</p></td>
        <td><p><keyseq><key>Ctrl</key><key>P</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>現在のドキュメントウィンドウを閉じる</p></td>
        <td><p><keyseq><key>Ctrl</key><key>W</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>ドキュメントの再読み込み (実際にはドキュメントを閉じて開き直す)</p></td>
        <td><p><keyseq><key>Ctrl</key><key>R</key></keyseq></p></td>
    </tr>
  </table>
</section>

<section id="move-around">
    <title>ドキュメント内の移動</title>
<table frame="all" rules="cols" shade="rows">
    <tr>
        <td><p>ページを上下に移動</p></td>
        <td><p>矢印キー</p></td>
    </tr>
    <tr>
        <td><p>一度に複数行ページを上下に移動</p></td>
        <td><p><key>Page Up</key> / <key>Page Down</key></p></td>
    </tr>
    <tr>
        <td><p>前後のページへ移動</p></td>
        <td><p><keyseq><key>Ctrl</key><key>Page Up</key></keyseq> / <keyseq><key>Ctrl</key><key>Page Down</key></keyseq></p></td>
    </tr>
<tr>
        <td><p>Go to page number.</p></td>
        <td><p><keyseq><key>Ctrl</key><key>L</key></keyseq> followed by the page number and <key>Enter</key></p></td>
</tr>
    <tr>
        <td><p>ページの冒頭へ移動 (<guiseq><gui>表示</gui><gui>連続ページ</gui></guiseq>が選択されている場合はドキュメントの冒頭)</p></td>
         <td><p><key>Home</key></p></td>
    </tr>
    <tr>
        <td><p>ページの末尾へ移動 (<guiseq><gui>表示</gui><gui>連続ページ</gui></guiseq>が選択されている場合はドキュメントの末尾)</p></td>
        <td><p><key>End</key></p></td>
    </tr>
    <tr>
    	<td><p>ドキュメントの冒頭へ移動</p></td>
        <td><p><keyseq><key>Ctrl</key><key>Home</key></keyseq></p></td>
    </tr>
    <tr>
    	<td><p>ドキュメントの末尾へ移動</p></td>
         <td><p><keyseq><key>Ctrl</key><key>End</key></keyseq></p></td>
    </tr>
    <tr>
    	<td><p>Display the side bar with a table of contents.</p></td>
         <td><p><key>F9</key></p></td>
    </tr></table>
</section>

<section id="copy">
    <title>テキストの選択とコピー</title>
<table frame="all" rules="cols" shade="rows">
    <tr>
        <td><p>ハイライトされたテキストのコピー</p></td>
        <td><p><keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>ドキュメント内のすべてのテキストを選択</p></td>
        <td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
    </tr>
</table>
</section>

<section id="find">
    <title>テキストの検索</title>
<table frame="all" rules="cols" shade="rows">
    <tr>
        <td><p>ドキュメント内の語句を検索可能なツールバーを表示する。このキーを押すと検索ボックスが自動的にハイライトされ、任意のテキストを入力すると検索を開始する。</p></td>
        <td><p><keyseq><key>Ctrl</key><key>F</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>次の検索結果へ移動</p></td>
        <td><p><keyseq><key>Ctrl</key><key>G</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>前の検索結果へ移動</p></td>
        <td><p><keyseq><key>Ctrl</key><key>Shift</key><key>G</key></keyseq></p></td>
    </tr>
</table>
</section>

<section id="rotate-zoom">
    <title>回転と拡大・縮小</title>
<table frame="all" rules="cols" shade="rows">
    <tr>
        <td><p>ページを反時計回りに 90 度回転</p></td>
        <td><p><keyseq><key>Ctrl</key><key>Left arrow</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>ページを時計回りに 90 度回転</p></td>
        <td><p><keyseq><key>Ctrl</key><key>Right arrow</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>拡大</p></td>
        <td><p><keyseq><key>Ctrl</key><key>+</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>縮小</p></td>
        <td><p><keyseq><key>Ctrl</key><key>-</key></keyseq></p></td>
    </tr>
</table>
</section>
</section>

</page>
