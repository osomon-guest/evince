<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shortcuts" xml:lang="cs">

  <info>
    <link type="guide" xref="index#advanced"/>
    <desc>Podívejte se na seznam všech klávesových zkratek.</desc>

    <revision pkgversion="3.2" version="0.1" date="2011-09-05" status="final"/>
    <revision pkgversion="3.24" version="0.2" date="2017-08-10" status="final"/>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
       <name>Tiffany Antopolski</name>
       <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Taufan Lubis</name>
       <email>taufanlinux@gmail.com</email>
    </credit>
   <credit type="editor">
       <name>Ronaldi Santosa</name>
       <email>ronald.santosa@gmail.com</email>
    </credit>
   <credit type="editor">
       <name>Ibnu Amansyah</name>
       <email>javalander9@gmail.com</email>
    </credit>
   <credit type="editor">
       <name>Andre Klapper</name>
       <email>ak-47@gmx.net</email>
    </credit>

    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Martin Picek</mal:name>
      <mal:email>picek.martin@gnome-cesko.cz</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz.</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  </info>


<title>Klávesové zkratky</title>

<section id="default-shortcuts"><title>Výchozí klávesové zkratky</title>
<section id="open-close-save-print">
  <title>Otevírání, zavírání, ukládání a tisk</title>
  <table frame="all" rules="cols" shade="rows">
    <tr>
        <td><p>Otevřít dokument</p></td>
        <td><p><keyseq><key>Ctrl</key><key>O</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>Otevřít kopii aktuálního dokumentu</p></td>
        <td><p><keyseq><key>Ctrl</key><key>N</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>Uložit kopii aktuálního dokumentu pod novým názvem souboru</p></td>
        <td><p><keyseq><key>Ctrl</key><key>S</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>Vytisknout aktuální dokument</p></td>
        <td><p><keyseq><key>Ctrl</key><key>P</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>Zavřít okno s aktuálním dokumentem</p></td>
        <td><p><keyseq><key>Ctrl</key><key>W</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>Znovu načíst dokument (ve skutečnosti dokument zavře a znovu otevře)</p></td>
        <td><p><keyseq><key>Ctrl</key><key>R</key></keyseq></p></td>
    </tr>
  </table>
</section>

<section id="move-around">
    <title>Pohyb po dokumentu</title>
<table frame="all" rules="cols" shade="rows">
    <tr>
        <td><p>Posun stránky nahoru/dolů</p></td>
        <td><p>Kurzorové šipky</p></td>
    </tr>
    <tr>
        <td><p>Posun stránky nahoru/dolů o několik řádků naráz</p></td>
        <td><p><key>Page Up</key>/<key>Page Down</key></p></td>
    </tr>
    <tr>
        <td><p>Přejít na předchozí/následující stránku</p></td>
        <td><p><keyseq><key>Ctrl</key><key>Page Up</key></keyseq>/<keyseq><key>Ctrl</key><key>Page Down</key></keyseq></p></td>
    </tr>
<tr>
        <td><p>Přejít na stránku číslo.</p></td>
        <td><p><keyseq><key>Ctrl</key><key>L</key></keyseq> následováno číslem stránky a <key>Enter</key></p></td>
</tr>
    <tr>
        <td><p>Přejít na začátek stránky (případně začátek dokumentu, kdy je vybráno <guiseq><gui>Zobrazit</gui> <gui>Souvislé</gui></guiseq>).</p></td>
         <td><p><key>Home</key></p></td>
    </tr>
    <tr>
        <td><p>Přejít na konec stránky (případně konec dokumentu, kdy je vybráno <guiseq><gui>Zobrazit</gui> <gui>Souvislé</gui></guiseq>).</p></td>
        <td><p><key>End</key></p></td>
    </tr>
    <tr>
    	<td><p>Přejít na začátek dokumentu</p></td>
        <td><p><keyseq><key>Ctrl</key><key>Home</key></keyseq></p></td>
    </tr>
    <tr>
    	<td><p>Přejít na konec dokumentu</p></td>
         <td><p><keyseq><key>Ctrl</key><key>End</key></keyseq></p></td>
    </tr>
    <tr>
    	<td><p>Zobrazit postranní panel s obsahem</p></td>
         <td><p><key>F9</key></p></td>
    </tr></table>
</section>

<section id="copy">
    <title>Výběr a kopírování textu</title>
<table frame="all" rules="cols" shade="rows">
    <tr>
        <td><p>Kopírovat zvýrazněný text</p></td>
        <td><p><keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>Vybrat všechen text v dokumentu</p></td>
        <td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
    </tr>
</table>
</section>

<section id="find">
    <title>Hledání textu</title>
<table frame="all" rules="cols" shade="rows">
    <tr>
        <td><p>Zobrazit lištu nástrojů sloužící k hledání slov v dokumentu. Po zmáčknutí této klávesové zkratky se vyhledávací políčko automaticky vybere a jakmile začnete psát text, hned se vyhledává.</p></td>
        <td><p><keyseq><key>Ctrl</key><key>F</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>Přejít na následující výsledek hledání</p></td>
        <td><p><keyseq><key>Ctrl</key><key>G</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>Přejít na předchozí výsledek hledání</p></td>
        <td><p><keyseq><key>Ctrl</key><key>Shift</key><key>G</key></keyseq></p></td>
    </tr>
</table>
</section>

<section id="rotate-zoom">
    <title>Otáčení a přibližování</title>
<table frame="all" rules="cols" shade="rows">
    <tr>
        <td><p>Otočit stránku o 90 stupňů proti směru hodinových ručiček</p></td>
        <td><p><keyseq><key>Ctrl</key><key>šipka vlevo</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>Otočit stránku o 90 stupňů po směru hodinových ručiček</p></td>
        <td><p><keyseq><key>Ctrl</key><key>šipka vpravo</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>Přiblížit</p></td>
        <td><p><keyseq><key>Ctrl</key><key>+</key></keyseq></p></td>
    </tr>
    <tr>
        <td><p>Oddálit</p></td>
        <td><p><keyseq><key>Ctrl</key><key>-</key></keyseq></p></td>
    </tr>
</table>
</section>
</section>

</page>
